// Soal 1
var readBooks = require("./callback.js");
var books = [
  { name: "LOTR", timeSpent: 3000 },
  { name: "Fidas", timeSpent: 2000 },
  { name: "Kalkulus", timeSpent: 4000 },
  { name: "komik", timeSpent: 1000 },
];
// Tulis code untuk memanggil function readBooks di sini
let bacaBuku = (time, books, index) => {
  if (index < books.length) {
    readBooks(time, books[index], (sisaWaktu) => {
      if (sisaWaktu > 0) {
        index += 1;
        bacaBuku(sisaWaktu, books, index);
      }
    });
  }
};
bacaBuku(10000, books, 0);
