var readBooksPromise = require("./promise.js");

var books = [
  { name: "LOTR", timeSpent: 3000 },
  { name: "Fidas", timeSpent: 2000 },
  { name: "Kalkulus", timeSpent: 4000 },
];

var time = 10000;
const bacaBuku = async (time, books) => {
  for (var i = 0; i < books.length; i++) {
    await readBooksPromise(time, books[i], (sisaWaktu) => {
      if (sisaWaktu[i] > 0) {
        time -= sisaWaktu;
        bacaBuku(time, books);
      }
    });
  }
};

bacaBuku(time, books)
  .then((result) => result)
  .catch((error) => error);
